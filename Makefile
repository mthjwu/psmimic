cc=gcc
CFLAGS= -fopenmp
prefix=/home/ct/trash/psMimic
perllib=/usr/share/perl5
SHELL:=/bin/bash

CPPFLAGS= $(LIBS)

OBJECTS = psR_TMsearch.o
SOURCES = psR_TMsearch.c

#PERLM_N=$(shell ls module)
#PERLM=$(addprefix module/, $(PERLM_N))

#------update 20120620--------------------
psRobot_mim=psRobot_mim.pl
psRobot_mim_tmp=$(basename $(psRobot_mim))
psRobot_mim_bin=${prefix}/psRobot_mim
#------update 20120620--------------------
psR_map_untreated=psR_map_untreated
psRobot_tar=psRobot_tar
#------update 20120620--------------------
psRobot_TMsearch=psR_TMsearch
#------update 20120620--------------------

#------update 20120620--------------------
psRobot_bin= $(psRobot_mim_tmp) $(psRobot_TMsearch)
#------update 20120620--------------------

all: _modifyPerlInMake _$(psRobot_TMsearch)
	@-/bin/chmod a+x $(psRobot_bin)
	@echo -e "\n\nmake \e[01;31msuccessed\e[00m. Please run \e[01;31mmake install\e[00m."

.c.o:
	$(cc) $(CFLAGS) -c -o $@ $< $(LIBS)

%.d: %.c
	@set -e; rm -f $@; \
	$(CC) -MM $(CPPFLAGS) $< > $@.$$$$; \
	sed 's,\($*\)\.o[ :]*,\1.o $@ : ,g' < $@.$$$$ > $@; \
	rm -f $@.$$$$ 

#include $(SOURCES:.c=.d)

	
#------update 20120620--------------------
_$(psRobot_TMsearch): psR_TMsearch.o
	$(cc) $(CFLAGS) -o $(psRobot_TMsearch) $^ $(LIBS)
#------update 20120620--------------------

_modifyPerlInMake:
#------update 20120620--------------------
	@/bin/sed '/^my $$lib_dir = .*;/d' $(psRobot_mim) >$(psRobot_mim_tmp)
	@/bin/sed -i '2 a\my $$lib_dir = \"/home/ct/trash/psMimic_v1.1\";' $(psRobot_mim_tmp)
#------update 20120620--------------------

_modifyPerlInMakeInstall:
#------update 20120620--------------------
	@/bin/sed -i '/^my $$lib_dir = .*;/d' $(psRobot_mim_tmp)
	@/bin/sed -i '2 a\my $$lib_dir = \"/home/ct/trash/psMimic/\" ;' $(psRobot_mim_tmp)
#------update 20120620--------------------


install: _modifyPerlInMakeInstall
	/bin/mkdir -p $(prefix)
	if [ "$(prefix)" != "/home/ct/trash/psMimic_v1.1" ]; then /bin/cp -f $(psRobot_bin) $(prefix); fi
	@-/bin/chmod a+x $(psRobot_bin)
	@-/bin/chmod a+x $(addprefix $(prefix)/, $(psRobot_bin))
	@if [ $$(whoami) != 'root' ]; then \
		echo 'export PATH=$(prefix):$${PATH}' >>~/.bashrc; \
		echo -e "\n\n Install finished! \n\nPlease run the following command to update your environment variable. \n\n***********\e[01;31msource $${HOME}/.bashrc\e[00m***********"; fi


uninstall: clean cleanBin
	/bin/rm -f $(addprefix $(prefix)/, $(psRobot_bin)) \
	    $(addprefix $(perllib)/, $(PERLM_N))
	if [ $$(whoami) != 'root' ]; then \
		sed -i 's#export PATH=$(prefix):.*##g' ~/.bashrc; \
		source ~/.bashrc; fi

clean:
	/bin/rm -f $(OBJECTS)
	/bin/rm -f *.d
	/bin/rm -f *.d*
cleanBin:
	/bin/rm -f $(psRobot_TMsearch)
	/bin/rm -f $(psRobot_mim_tmp)
.PHONY:clean cleanBin install uninstall 
