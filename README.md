# psMimic #
--------
Software used in the article: [Widespread long noncoding RNAs as endogenous target mimics for microRNAs in plants](http://www.plantphysiol.org/content/161/4/1875.full).

psMimic is designed to search potential small RNA target mimics on a large scale genomic context. The package is written in C with a Perl wrapper. In order to install and use the local version of psMimic, you will need a Unix/Linux system, a C compiler and a Perl interpreter.

Please find the online tools and stand-alone software package in the following link: http://omicslab.genetics.ac.cn/psMimic/

The same stand-alone software package also deposits here.

### DOWNLOAD ###
--------
Download the pipeline using git clone or using the "download" link.

     git clone https://bitbucket.org/mthjwu/psmimic

### REQUIREMENTS ###
--------
1. perl 5 or greater
2. gcc 4.0 or greater

### INSTALLATION ###
--------
The simplest way to install this package is (require root permission):

     ./configure
     make
     make install (log as root)
     
 Install psRobot in an alternative path with no root permission:

     ./configure -p <your path> -l <your path>
     make
     make install (log as root)
 
 Configure options:

     -h			display this help and exit
     -p PREFIX	install architecture-independent files in PREFIX [complete path needed, default /usr/bin]
     -l LIBS	install perl libs in LIBS [complete path needed, default standard perl lib, the path next to last in your @INC]

 Uninstall psRobot:

     sudo make uninstall

### TEST RUN ###
To test psMimic:

     cd test/psRobot_mim
     psRobot_mim -s smRNA -t target

Compare the output file of each command to the *.standard file in each directory.
They should be identical, if psRobot is properly installed.

### Who do I talk to? ###
--------
* Hua-Jun Wu (hjwu@jimmy.harvard.edu)




