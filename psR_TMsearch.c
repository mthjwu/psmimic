#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include "string.h"
#define TAGLEN 1000
#define SMRNA 100
#define TARGETNUM 1000000
#define GRID 10000
#define CUTOFFSCORE 3
#define THREEPRIME 31
#define GAPSTT 9
#define GAPEND 11
#define FIVEPRIME 1
#define BUFFER 1000000
#define GAPNUM 3

// declare sub-function
int BP (char nt1, char nt2);
void reverse (char seq[]);
void SW (char qry[], char ref[], char qry_tag[], char ref_tag[], FILE *output);
int max (int a, int b);
void shift (char array[]);
void UtoT (char array[]);
// score matrix
typedef struct cs
{
     float  score;
     char   last;
     char   tag;
}cross;

typedef struct db
{
     char *id;
     char *seq;
}database;

float  par_target_score = CUTOFFSCORE;
int  par_five_prime   = FIVEPRIME;
int  par_three_prime  = THREEPRIME;
int  par_gap_stt      = GAPSTT;
int  par_gap_end      = GAPEND;
int  par_thread_num   = 1;
int  par_gap_num      = GAPNUM;

main(int argc, char *argv[])
{
      // judge par
     if (argc == 1 || (argc == 2 && !strcmp(argv[1],"-h"))){
          printf("psRobot_tar arguments: \n\n");
          printf("  -s    input file name: smRNA sequences (fasta format)\n");
          printf("    default = smRNA\n");
          printf("  -t    input file name: target sequences (fasta format)\n");
          printf("    default = target\n");
          printf("  -o    output file name\n");
          printf("    default = smRNA-target.pTM\n");
          printf("  -ts   target score, lower is better (0-5)\n");
          printf("    default = 3\n");
          printf("  -fp   5' prime boundary of essential sequence (1-2)\n");
          printf("    default = 1\n");
          printf("  -tp   3' prime boundary of essential sequence (15-31)\n");
          printf("    default = 31\n");
          printf("  -cs   position after which is the supposed cleavage site (1-30)\n");
          printf("    default = 10\n");
          printf("  -p    number of processors to used\n");
          printf("    default = 1\n");
          printf("  -gn   number of gaps permitted (1-5)\n");
          printf("    default = 3\n");
          printf ("\nhuajun\nPowered by omicslab\n");
          return (0);
     }

     if (argc%2 == 0) printf("ERROR: Missing parameters.\n");
     
     int par_num;
     char par_rna_file[100];
     char par_chr_file[100];
     char par_out_file[100];
     strcpy(par_rna_file, "smRNA");
     strcpy(par_chr_file, "target");
     strcpy(par_out_file, "smRNA-target.pTM");

     for (par_num=1; par_num<argc; par_num=par_num+2){
          if (!strcmp(argv[par_num],"-s")){
               strcpy(par_rna_file, argv[par_num+1]);
          }else if (!strcmp(argv[par_num],"-t")){
               strcpy(par_chr_file, argv[par_num+1]);
          }else if (!strcmp(argv[par_num],"-o")){
               strcpy(par_out_file, argv[par_num+1]);
          }else if (!strcmp(argv[par_num],"-ts")){
               par_target_score = atof(argv[par_num+1]);
          }else if (!strcmp(argv[par_num],"-fp")){
               par_five_prime   = atoi(argv[par_num+1]);
          }else if (!strcmp(argv[par_num],"-tp")){
               par_three_prime  = atoi(argv[par_num+1]);
          }else if (!strcmp(argv[par_num],"-cs")){
               par_gap_stt      = atoi(argv[par_num+1])-1;
               par_gap_end      = atoi(argv[par_num+1])+1;
          }else if (!strcmp(argv[par_num],"-p")){
               par_thread_num   = atoi(argv[par_num+1]);
          }else if (!strcmp(argv[par_num],"-gn")){                                                                       
               par_gap_num      = atoi(argv[par_num+1]);
          }else{
               printf("Command line help: ./psRobot_mim -h\n");
               exit (1);
          }
     }      
     
     par_target_score = par_target_score+0.5*par_gap_num; 

      FILE *rna_file, *chr_file, *out_file;
      char rna_tag[TAGLEN];
	  char rna_seq[SMRNA];
	  char *chr_tag;
	  char *chr_seq;
      
      // open output file
      if ((out_file = fopen (par_out_file,"wb+")) == NULL)
	  {
		  printf ("Cannot edit output file\n");
		  return (0);
	  }


      // read target sequence data
                 if ((chr_file = fopen (par_chr_file,"rt")) == NULL)
	         {
		      printf ("Cannot open target file\n");
                      return (0);
                 }
                 int chr_n = -1;
                 char *chr_line;
                 if ( (chr_line = (char *) malloc (BUFFER*sizeof(char))) == NULL ){
                      printf("Cannot get the memory!\n");
                      return (0);
                 }
                 database *ref_seq;
                 if ( (ref_seq = (database *) malloc (TARGETNUM*sizeof(database))) == NULL ){
                      printf("Cannot get the memory!\n");
                      return (0);
                 }
                 if ( (chr_seq = (char *) malloc (BUFFER*sizeof(char))) == NULL ){
                      printf("Cannot get the memory!\n");
                      return (0);
                 }
                 if ( (chr_tag = (char *) malloc (BUFFER*sizeof(char))) == NULL ){
                      printf("Cannot get the memory!\n");
                      return (0);
                 }
                 while (1)
                 {
                      memset(chr_line, 0, sizeof(chr_line));
                      char *chr_jd = fgets(chr_line,BUFFER,chr_file);
                      int chr_line_len;
                      chr_line_len = strlen(chr_line)-1;
                      if (chr_line[chr_line_len] == '\n')
                      {
                           chr_line[chr_line_len] = '\0';                                
                      }
                      if (chr_line[0] == '>' || !chr_jd)
                      {
                           shift(chr_line);
                           if (chr_n >= 0)
                           {
                              if ( (ref_seq[chr_n].seq = (char *) malloc ((strlen(chr_seq)+1)*sizeof(char))) == NULL ){
                                   printf("Cannot get the memory!\n"); return (0);
                              }
                              if ( (ref_seq[chr_n].id  = (char *) malloc ((strlen(chr_tag)+1)*sizeof(char))) == NULL ){
                                   printf("Cannot get the memory!\n"); return (0);
                              }
                              strcpy(ref_seq[chr_n].id, chr_tag);
                              strcpy(ref_seq[chr_n].seq, chr_seq);
                           }
                           memset(chr_tag, 0, sizeof(chr_tag));
                           memset(chr_seq, 0, sizeof(chr_seq));
                           chr_n++;
                           strcat(chr_tag,chr_line);
                      }
                      else
                      {
                           strcat(chr_seq, chr_line);
                      }
                      if (!chr_jd)
                      {
                           break;           
                      }
                 }
                 
                 fclose (chr_file);      


      // read smRNA file
      if ((rna_file = fopen (par_rna_file,"rt")) == NULL)
	  {
		  printf ("Cannot open smRNA file\n");
		  return (0);
	  }
      
      char rna_line[TAGLEN];
      int rna_n = -1;

      while (1)
	  {
            char *rna_jd = fgets(rna_line,TAGLEN,rna_file);
            if (rna_line[strlen(rna_line)-1] == '\n')
            {
                 rna_line[strlen(rna_line)-1] = '\0';                                
            }
            if (rna_line[0] == '>' || !rna_jd)
            {
               if (rna_n >= 0)
               { 
                    int ref_count;
                    UtoT(rna_seq);
                    #pragma omp parallel for num_threads(par_thread_num)
                    for (ref_count=0; ref_count<chr_n; ref_count++){
                         SW(rna_seq, (*(ref_seq+ref_count)).seq, rna_tag, (*(ref_seq+ref_count)).id, out_file);
                    }
               }
              memset(rna_tag, 0, sizeof(rna_tag));
              memset(rna_seq, 0, sizeof(rna_seq));
              rna_n++;
              strcat(rna_tag, rna_line);           
            }
            else
            {
                 strcat(rna_seq, rna_line);
            }
            if (!rna_jd)
            {
                 break;           
            }
      }
	  
      fclose (rna_file);
      fclose (out_file);


}
      
void SW (char qry[], char ref[], char qry_tag[], char ref_tag[], FILE *output){

      char ref_rev[strlen(ref)];
      strcpy(ref_rev, ref);
      reverse(ref_rev);
      
      // par
      int i = 0;
      int j = 0;
      int qry_len = strlen(qry);
      int ref_len = strlen(ref_rev);
      
      // scoring scheme
      int   match    = 3;
      int   gumatch  = 1;
      int   mismatch = -1;
      float gap_ref  = -1.1;
      int   gap_qry  = -4;
      

      // circle for matrix calulation
      int   grid_judge   = 1;
      int   grid_stt     = 1;
      int   grid_end     = 1;
      float target_score = par_target_score+1;
      
      while(grid_judge)
      {
     
      grid_end   = grid_stt+GRID-1;
      if (grid_end >= ref_len)
      {
           grid_end   = ref_len;
           grid_judge = 0;            
      }
      int  grid_len   = grid_end-grid_stt+1;
      
      cross matrix[grid_len+1][qry_len+1];
      
      // initialization
      for (i=0; i<grid_len+1; i++)
      {
          matrix[i][0].score = 0;
          matrix[i][0].last  = 'n';
      }
      for (j=0; j<qry_len+1; j++)
      {
          matrix[0][j].score = 0;
          matrix[0][j].last  = 'n';
      }
      
      // fill
      
      int   max_i       = 0;
      int   max_j       = 0;
      float   max_score = 0;
      int   max_near    = 0;
      int   break_delay = 0;

      for (i=1; i<grid_len+1; i++){
         for (j=1; j<qry_len+1; j++){
             float diagonal_score, left_score, up_score;
             
             // calculate match score
             int  match_judge = BP(qry[j-1],ref_rev[i-1+grid_stt-1]);
             char tag;
             if (match_judge == 1)
             {
                 diagonal_score = matrix[i-1][j-1].score + match;
                 tag = '|';            
             }
             else if (match_judge == 2)
             {
                 if (j > par_three_prime)
                 {
                      diagonal_score = matrix[i-1][j-1].score + 2*gumatch;
                 }
                 else if (j < par_five_prime)
                 {
                      diagonal_score = matrix[i-1][j-1].score + 2*gumatch;
                 }
                 else
                 {
                      diagonal_score = matrix[i-1][j-1].score + gumatch;
                 }
                 tag = ':'; 
             }
             else
             {
                 if (j > par_three_prime)
                 {
                      diagonal_score = matrix[i-1][j-1].score + (2+mismatch);
                 }
                 else if (j < par_five_prime)
                 {
                      diagonal_score = matrix[i-1][j-1].score + (2+mismatch);
                 }
                 else
                 {
                      diagonal_score = matrix[i-1][j-1].score + mismatch;
                 }
                 tag = '*';    
             }
             
             // calculate gap score
             if (par_gap_stt != 0 && j >= par_gap_stt && j <= par_gap_end){
                     up_score     = matrix[i-1][j].score + gap_qry/2;
                     left_score   = matrix[i][j-1].score + (gap_ref-par_target_score*4);
             }else {
                 up_score     = matrix[i-1][j].score + (par_target_score+1)*gap_qry;
                 left_score   = matrix[i][j-1].score + (gap_ref-par_target_score*4);
             }

             // choose best score
             if (diagonal_score >= up_score) {
                if (diagonal_score >= left_score) {
                   matrix[i][j].score   = diagonal_score;
                   matrix[i][j].last    = 'd';
                   matrix[i][j].tag     = tag;
                }
                else 
                {
                   matrix[i][j].score   = left_score;
                   matrix[i][j].last    = 'l';
                }
             }
             else
             {
                if (up_score >= left_score) {
                   matrix[i][j].score   = up_score;
                   matrix[i][j].last    = 'u';
                }
                else
                {
                   matrix[i][j].score   = left_score;
                   matrix[i][j].last    = 'l';
                }
             }
         }    
             // set max score
             if (matrix[i][j-1].score >= max_score) // j already ++, so we must use j-1 instead of j
             {
                 max_i = i;
                 max_j = j-1;
                 break_delay = 0;
                 max_score = matrix[i][j-1].score;
                 int   temp01 = qry_len*match-max_score;
                 float temp02 = temp01;
                 target_score = temp02/4;
                 if (target_score <= par_target_score){
                      max_near = 1;
                 }
             }else{
                 if (max_near == 1){
                      break_delay++;
                      if (break_delay > 10){
                          grid_judge = 1;
                          break;
                      }
                 }
             }
         
      }
      
      
      //trace back

      if (target_score > par_target_score)
      {
           grid_stt = grid_end-qry_len;
           continue;
      }

      char align1[50];
      char align2[50];
      char align3[50];
      
      int n = 0;
      j = max_j;
      i = max_i;
      int gap_num = 0;

      while (matrix[i][j].last != 'n')
      {
           if (matrix[i][j].last == 'd')
           {
                align1[n] = qry[j-1];
                align2[n] = ref_rev[i-1+grid_stt-1];
                align3[n] = matrix[i][j].tag;
                j--; i--;
           }
           else if (matrix[i][j].last == 'l')
           {
                gap_num++;
                align1[n] = qry[j-1];
                align2[n] = '-';
                align3[n] = '*';
                j--;   
           }
           else if (matrix[i][j].last == 'u')
           {
                gap_num++;
                align1[n] = '-';
                align2[n] = ref_rev[i-1+grid_stt-1];
                align3[n] = '*';
                i--;
           }
           n++;
            
      }

      // Gap number judgement
      if (gap_num != par_gap_num){
           grid_stt = max_i+grid_stt-1; // reset grid start
           continue;
      }

      align1[n] = '\0';
      align2[n] = '\0';
      align3[n] = '\0';
      
      reverse(align1);
      reverse(align2);
      reverse(align3);
      
      int   qry_stt      = j+1;
      int   qry_end      = max_j;
      int   ref_stt      = ref_len-(i+grid_stt-1)+1-1;
      int   ref_end      = ref_len-(max_i+grid_stt-1)+1;
      
      fprintf(output, "%s\tScore: %.1f\t%s\n\nQuery: %10d %s %d\n       %10s %s\nSbjct: %10d %s %d\n\n\n", qry_tag, target_score, ref_tag, qry_stt, align1, qry_end, "", align3, ref_stt, align2, ref_end);
      
      grid_stt = max_i+grid_stt-1; // reset grid start
      } //while (grid_judge)
}


int BP (char nt1, char nt2)
{
    int nt_int = toupper(nt1)+toupper(nt2);
    if (nt_int == 149 || nt_int == 138)
    {
     	 return 1;                                                                                                   
    }                                                                                                                    
    else if (nt_int == 155){                                                                                       
         return 2;                                                                                                   
    }                                                                                                                    
    return 0;                                                                                                            
}

void reverse (char seq[])
{
     int seq_len = strlen(seq);
     int cnt, temp;
     for (cnt=0; cnt<seq_len/2; cnt++)
     {
         temp               = seq[cnt];
         seq[cnt]           = seq[seq_len-cnt-1];
         seq[seq_len-cnt-1] = temp;
     }
}

void shift (char array[])
{
     int array_len = strlen(array);
     int cnt;
     for (cnt=1; cnt<=array_len; cnt++)
     {
         array[cnt-1] = array[cnt];
     }
}

void UtoT (char array[])
{
     int array_len = strlen(array);
     int cnt;
     for (cnt=0; cnt<array_len; cnt++)
     {
         if (array[cnt] == 'U')
         {
             array[cnt] = 'T';
         }
     }
}





