#!/usr/bin/perl

use strict;

## Parameters Initiation
my $inFile01      = "smRNA";
my $inFile02      = "target";
my $outFile       = "smRNA-target.pTM";
my $targetScore   = 4;
my $cleavageSite  = 10;
my $numberCPU     = 1;
my $numberGap     = 3;
my $numberNM      = 4; # Suggest to set to $targetScore+1
my $numberMM      = 2; # Suggest to set to $targetScore-1
my $seed          = 8;
my $nonPerfect    = 1; 
my $seedNM        = 2;

## Input Parameters
if (@ARGV == 0 or (@ARGV == 1 and $ARGV[0] eq "-h")){
	print "psRobot_mim version 1.1\n";
	print "psRobot_mim is designed to find potential small RNA target mimics on a large scale.\n\n";
	print "Scoring rules: \n\n";
	print "  1.    Mismatches, bulges are evaluated with a penalty of +1.\n";
	print "  2.    G:U pairs are evaluated a penalty of +0.5.\n";
	print "  3.    Gaps are not permitted in mimic sequences.\n";
	print "  4.    Two-fold penalty scores will be evaluated in positions 2-8 and up/down-stream 2-nt positions flanking the cleavage site.\n";
	print "  5.    Only one mismatch is permitted in up/down-stream 2-nt positions flanking the cleavage site.\n";
	print "  6.    If all bulges are not perfectly occurred in the cleavage site, it will get a penalty of +1.\n\n";
	print "psRobot_mim arguments: \n\n";
	print "  -s    input file name: smRNA sequences (fasta format)\n";
	print "    default = smRNA\n";
	print "  -t    input file name: target mimicry sequences (fasta format)\n";
	print "    default = target\n";
	print "  -o    output file name\n";
	print "    default = smRNA-target.pTM\n";
	print "  -ts   target penalty score, lower is better (0-7)\n";
	print "    default = 4\n";
	print "  -cs   position after which is the supposed cleavage site (2-30)\n";
	print "    default = 10\n";
	print "  -p    number of processors to use\n";
	print "    default = 1\n";
	print "  -gn   number of bulges in up/down-stream 2-nt positions flanking the cleavage site (1-5)\n";
	print "    default = 3\n";
	print "  -nm   total number of mismatches/G:U pairs (0-7), we suggest users to set it to targetPenaltyScore+1\n";
	print "    default = 4\n";
	print "  -mm   total number of mismatches (0-7), , we suggest users to set it to targetPenaltyScore-1\n";
	print "    default = 2\n";
	print "  -snm  total number of mismatches/G:U pairs in seed region, here represents positions 2-8 (0-7) \n";
	print "    default = 2\n";
	print "\nWritten by Wu Hua-Jun\nPowered by omicslab\n";
	die   "\n";
}

(@ARGV%2 == 0) or die "ERROR: Missing parameter.\n";
my $par;
for ($par=0; $par<$#ARGV; $par+=2){
	if     ($ARGV[$par] eq "-s"){	 # Input file: smRNA sequences
		$inFile01     = $ARGV[$par+1];
	}elsif ($ARGV[$par] eq "-t"){	 # Input file: target sequences
		$inFile02     = $ARGV[$par+1];
	}elsif ($ARGV[$par] eq "-o"){
		$outFile      = $ARGV[$par+1];
	}elsif ($ARGV[$par] eq "-ts"){
		$targetScore  = $ARGV[$par+1];
	}elsif ($ARGV[$par] eq "-cs"){
		$cleavageSite = $ARGV[$par+1];
	}elsif ($ARGV[$par] eq "-p"){
		$numberCPU    = $ARGV[$par+1];
	}elsif ($ARGV[$par] eq "-gn"){
		$numberGap    = $ARGV[$par+1];
	}elsif ($ARGV[$par] eq "-nm"){
		$numberNM     = $ARGV[$par+1];
	}elsif ($ARGV[$par] eq "-mm"){
		$numberMM     = $ARGV[$par+1];
	}elsif ($ARGV[$par] eq "-snm"){
		$seedNM       = $ARGV[$par+1];
	}else{
		die "Command line help: psRobot_mim -h\n";
	}
}

## Pre Search
my $tmp_out = ".$inFile01-$inFile02";
$tmp_out =~ s/\///g;
$tmp_out =~ s/\~//g;
my $tmp_targetScore = $targetScore-1;
`$lib_dir/psR_TMsearch -s $inFile01 -t $inFile02 -o $tmp_out -ts $tmp_targetScore -cs $cleavageSite -p $numberCPU -gn $numberGap`;

## Fine Search
open IN  ,  $tmp_out or die $!;
open OUT , ">$outFile" or die $!;
$/ = ">";
while (<IN>){
	chomp;
	next if ($. == 1);
	my @lines = split /\n/,$_;
	my @score = split /\t/,$lines[0];
	my $qry = (split /\s+/,$lines[2])[2];
	my $ref = (split /\s+/,$lines[4])[2];
	my $sig = (split /\s+/,$lines[3])[1];
	my $qry_num   =  $qry =~ tr/-/-/;
	my $ref_num   =  $ref =~ tr/-/-/;
	my $sig_num   =  $sig =~ tr/\*\:/\*\:/;
	my $sig_mm    =  $sig =~ tr/\*/\*/;
	my @nogap_qry = split //,$qry;
	my @nogap_sig = split //,$sig;
	foreach my $i (0..$#nogap_qry){
		if ($nogap_qry[$i] eq '-'){
			$nogap_qry[$i] = "@";
			$nogap_sig[$i] = "@";
		}
	}
	my $nogap_qry = join("", @nogap_qry);
	my $nogap_sig = join("", @nogap_sig);
	$nogap_qry =~ s/@//g;
	$nogap_sig =~ s/@//g;
	
	## seed region non-match control
	my $seed_region = substr($nogap_sig, 1, $seed-1);
	my $seed_nm     =~ tr/\*\:/\*\:/;
	next if ($seed_nm > $seedNM);
	
	## Two-fold penalty control.
	my $double_sig;
	if ($cleavageSite <= $seed+2){ # Two-fold penalty control.
		if ($cleavageSite+1 > $seed-1){
			$double_sig = substr($nogap_sig, 1, $cleavageSite+1);
		}else{
			$double_sig = substr($nogap_sig, 1, $seed-1);
		}
	}else{
		$double_sig = substr($nogap_sig, 1, $seed-1).substr($nogap_sig, $cleavageSite-2, 4);
	}
	my $double_sig_mm = $double_sig =~ tr/\*/\*/;
	my $double_sig_gu = $double_sig =~ tr/\:/\:/;
	my $cleave_sig    = substr($nogap_sig, $cleavageSite-2, 4);
	my $cleave_sig_mm = $cleave_sig =~ tr/\*/\*/;

	next if ($cleave_sig_mm > 1); # cleavage site mismatch control.
	my $double_penalty = $double_sig_mm+$double_sig_gu/2;
	
	my @score_sub = split / /,$score[1];
	$score_sub[1] = $score_sub[1]-$qry_num/2+$double_penalty;
	$score[1] = "Score: ".$score_sub[1];
	$lines[0] = join("\t", @score);
	
	## Drop bad ones
	unless ($qry_num == $numberGap && $ref_num == 0){ #gap control
		next; print "111\n";
	}
	
	## Adjust the bulge site to its proper location
	my $adj_pos = 0;
	my $judge   = 0;
	my $pos11   = 0;
	for my $i (0..length($qry)){
		$adj_pos++ if (substr($qry, $i, 1) ne '-');
		if ($adj_pos == $cleavageSite-1){
			$judge++;
		}elsif ($adj_pos == $cleavageSite){
			my $comp_sd = substr($ref, $i-$judge+1, 1);
			my $comp_10 = substr($qry, $i, 1);
			if ( ($comp_sd eq 'T' and $comp_10 eq 'G') or ($comp_sd eq 'G' and $comp_10 eq 'T') ){
				$score_sub[1]=$score_sub[1]+1 if (substr($sig, $i, 1) eq '|'); # Change score
				($qry, $sig) = &_CROSSOVER($qry, $sig, $i-$judge+1, $i, 1);
				next;
			}
			my $dst = $i;
			for (my $j=$dst-1;$j>$i-$judge;$j--){
				if (substr($ref, $j, 1) eq substr($ref, $dst, 1)){
					($qry, $sig) = &_CROSSOVER($qry, $sig, $j, $dst, 0);
					$dst = $j;
				}
			}
		}elsif ($adj_pos == $cleavageSite+1){
			$pos11++;
		}elsif ($adj_pos == $cleavageSite+2){
			my $comp_sd = substr($ref, $i-1, 1);
			my $comp_11 = substr($qry, $i-$pos11, 1);
			if ( ($comp_sd eq 'T' and $comp_11 eq 'G') or ($comp_sd eq 'G' and $comp_11 eq 'T') ){
				$score_sub[1]=$score_sub[1]+1 if (substr($sig, $i-$pos11, 1) eq '|'); # change score
				($qry, $sig) = &_CROSSOVER($qry, $sig, $i-1, $i-$pos11, 1);
			}
		}
	}
	
	## Use blank instead of bulge mark "*", and use "o" to instead mismatch mark "*".
	my @qry = split //,$qry;
	my @sig = split //,$sig;
	for my $i (0..$#qry){
		if ($qry[$i] eq '-'){
			$sig[$i] = " ";
		}else{
			$sig[$i] = "o" if ($sig[$i] eq '*');
		}
	}
	$sig = join("", @sig);

	## Overwrite the alignment
	$lines[2] =~ m/^(.*\d+ )\S+( \d+.*)$/;
	$lines[2] = $1.$qry.$2;
	$lines[3] =~ m/^(\s+)\S+(.*)$/;
	$lines[3] = $1.$sig.$2;

	## Mark the cleaveage site:
	$lines[2] =~ m/^(.*\d+ )(\w+.*)$/;
	my $mark_head_len = length($1);
	my $mark_tail = $2;
	my $mark_pos_10 = 0;
	my $mark_pos_11 = 0;
	my $pos = 0;
	for my $i (0..length($mark_tail)){
		$pos++ if (substr($mark_tail, $i, 1) ne '-');
		if ($pos == $cleavageSite){
			$mark_pos_10 = $i unless ($mark_pos_10);
		}elsif ($pos == $cleavageSite+1){
			$mark_pos_11 = $i unless ($mark_pos_11);
		}
	}
	$lines[1] = (' 'x($mark_head_len+$mark_pos_10)).'v'.(' 'x($mark_pos_11-$mark_pos_10-1)).'v';
	
	my $perfect_bulge = $mark_pos_11-$mark_pos_10-1;
	unless ($perfect_bulge == $numberGap){ # non-perfect bulge will get a "$nonPerfect" score penalty.
		$score_sub[1] = $score_sub[1]+$nonPerfect;
		$score[1] = "Score: ".$score_sub[1];
		$lines[0] = join("\t", @score);
	}
	
	next unless ($score_sub[1] <= $targetScore); ## target penalty score control.
	next unless ($sig_num-$qry_num <= $numberNM and $sig_mm-$qry_num <= $numberMM); ## number of non-match control.

	my $key_bulge;
	while ($qry =~ m/(\-+)/g){
		$key_bulge = length($1) if (length($1) > $key_bulge);
	}

	next if ( $mark_pos_11-$mark_pos_10-1 == 0 and $key_bulge != $numberGap); # extra bulge control, not detailed in the parameter. Del pattern like "-AT--".
	
	if ( ($numberGap <= 2 and $key_bulge == $numberGap) or ($numberGap > 2 and $key_bulge >= $numberGap-1) ){ # bulge control
		$_ = join("\n", @lines);
		print OUT ">$_\n\n\n";
	}
}
close IN;
close OUT;


sub _CROSSOVER{
	(my $_qry, my $_sig, my $_j, my $_dst, my $_jdg) = @_;
	my $exchange = substr($_qry, $_j, 1);
	substr($_qry, $_j, 1) = substr($_qry, $_dst, 1);
	substr($_qry, $_dst, 1) = $exchange;
	$exchange = substr($_sig, $_j, 1);
	substr($_sig, $_j, 1) = substr($_sig, $_dst, 1);
	substr($_sig, $_dst, 1) = $exchange;
	substr($_sig, $_j, 1) = ':' if ($_jdg == 1);
	return($_qry, $_sig);
}




